-- creation de la DB
CREATE SCHEMA IF NOT EXISTS db_chat /* !40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ ;
USE db_chat;

-- creation des tables 

CREATE TABLE IF NOT EXISTS users (
	user_id INT AUTO_INCREMENT PRIMARY KEY,
	user_name VARCHAR (50) NOT NULL,
   	pseudo VARCHAR (50) NOT NULL
	-- user_pwd VARCHAR (12), -- NOT NULL,
	-- date_user TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS msg (
	msg_id INT AUTO_INCREMENT PRIMARY KEY,
	msg_body VARCHAR (255) NOT NULL, -- text ou varchar ? 
	-- date_msg TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id INT NOT NULL
    -- constraint foreign key
);
CREATE UNIQUE INDEX uidx_pseudo
ON users (pseudo); 


ALTER TABLE msg
ADD FOREIGN KEY fk_users_msg(user_id)
REFERENCES users(user_id)
-- ON DELETE CASCADE
-- ON UPDATE CASCADE
;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ishai';


-- ALTER TABLE msg DROP FOREIGN KEY msg_ibfk_1;

-- INSERT INTO users (user_name, pseudo) VALUE ('1','fdz');
SELECT * FROM users;

-- INSERT INTO msg (user_id, msg_body) VALUE ('1', 'frfr');
-- SELECT * FROM msg;

-- DELETE FROM users
-- WHERE user_id = 1;
