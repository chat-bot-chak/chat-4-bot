-- Qry 1 : Create user 
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ishai';
INSERT INTO users(user_name, pseudo, user_pwd)
VALUES('user_name','pseudo','user_pwd') -- deux users ne peuvent pas avoir un même user_name (?
      ;

-- Qry 2 : delete user

DELETE FROM `users`
WHERE user_id = user_id;


-- Qry 3 : MAJ user_pwd

UPDATE users
SET user_pwd = 'new_user_pwd', user_name = 'soitExistant_soitNew_user_name'
WHERE user_id = user_id

-- Qry 4 : MAJ user_name

UPDATE users
SET user_name = 'new_user_name'
WHERE user_id = user_id



-- Qry 5 : User post msg
INSERT INTO msg(msg_body, user_id)
VALUES("msg_body", "user_id") -- TRES IMPORTANT DE METTRE LE USER_ID
WHERE user_id = user_id



-- Qry 6 : User get 10 last msg

select msg_body
from msg
LIMIT 10
ORDER BY msg_id DESC;

-- Qry 7 : User get liste des utilsateurs
select user_name
FROM users;



