# Chat 4 Bot

Chat 4 Bot est une application web de chat

L'application permet d'identifier des profils utilisateurs et de conserver les échanges dans une base de données.
elle permet aussi de consulter/afficher les 10 derniers messages qui ont été enregistrés en allant du plus récent au plus ancien.

Le dossier Maquette permet de visuliser son architecture, l'organisation de la Base de données et la representation des pages front.
Pour les maquettes nous avons utilisé Draw.io et Marvel App.

Nous avons utilisé Node.js sous Visual Studio et MySQL Workbench. 


Nous avons utilisé Trello pour l'organisation des taches : https://trello.com/b/x6NhlsoX/chat-4-bot

