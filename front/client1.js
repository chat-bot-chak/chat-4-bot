const express = require('express')
const axios = require('axios')
const bodyParser = require('body-parser')
//const urlencodedParser = bodyParser.urlencoded({ extended: false })
const  port = 8005;
const app = express();

app.set('view engine', 'ejs')
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', express.static('public'))

let lol = ""

//pour afficher la page d'inscription
app.get('/', function(req, res){
  res.render('inscription.ejs');
})



// start pour envoyer les données a api
app.post('/', (req, res) => {
     // console.log(req.body);
     axios.post('http://localhost:8006/user', req.body )
      .then(function (resp) {
        res.redirect('list');
        res.send();
        })
        .catch(function (error) {
     });
});
// END pour envoyer les données a api


// start pour get les données from api
app.get('/list', function(req, res){
  axios.get('http://localhost:8006/list')
    .then (function  (resp) {
      console.log('hahaha');
      res.render('list.ejs', { users : resp.data });
    })
    .catch(function (error) {
    });
      // res.send(res);
});
// END pour get les données from api

 
// start pour SUPPRIMER les données a api
app.get('/list/:user_id', (req, res) => {
    let id = req.params.user_id;
    console.log('test' + id);
    axios.delete('http://localhost:8006/list/' + id )
      .then(function (resp) {
        res.redirect('list.ejs');
        res.end();                               
      })
      .catch(function (error) {
      });
    
});
 // END pour SUPPRIMER les données a api



// PAGE LOGIN
//pour afficher la page de logIN
app.get('/logIn', function(req, res){
  res.render('logIn');
})

// start pour envoyer logIN données a api
app.post('/logIn', (req, res) => {
    axios.post('http://localhost:8006/logIn', req.body )
    .then(function (resp) {
      console.log('ici');
      lol = resp.data.user_id;
      console.log(resp.data.user_id);
    // console.log("*****resp****", resp.user_id);
        res.redirect('message');
        res.send();
    }).catch(function (error) {
        console.log(error.message)
        console.log('ERROOOOOOOR')
    });
});




// END pour envoyer les données a api


// PAGE MESSAGE
// start pour envoyer message  a api
app.post('/message', (req, res) => {
  console.log(req.body);
      axios.post('http://localhost:8006/msg', req.body )
      .then(function (resp) {
        // console.log("****************************************"+res[0].user_id)
        res.render('message.ejs', {message: resp.data, userId: lol });
       })
      .catch(function (error) {
       });
});
// END pour envoyer les données a api

app.get('/message', function(req, res){
  axios.get('http://localhost:8006/msg')
    .then (function  (resp) {
      // console.log(resp);
      res.render('message.ejs', {message: resp.data, userId: lol });
    })
    .catch(function (error) {
    });
    
});

app.listen(8005);